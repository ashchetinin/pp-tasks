import { Component, OnInit } from '@angular/core';
import {of} from 'rxjs';
import {ApiService} from '../../core/services/api.service';

@Component({
  selector: 'app-shop-window',
  templateUrl: './shop-window.component.html',
  styleUrls: ['./shop-window.component.css']
})
export class ShopWindowComponent implements OnInit {

  public product$;

  constructor(
    private api: ApiService
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.product$ = this.api.getProductList();
  }

}
