import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopWindowComponent } from './shop-window/shop-window.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { ShopRoutingModule } from './shop-routing.module';



@NgModule({
  declarations: [
    ShopWindowComponent,
    ProductItemComponent
  ],
  imports: [
    CommonModule,
    ShopRoutingModule
  ]
})
export class ShopModule { }
