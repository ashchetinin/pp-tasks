import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: 'login', data: { preload: true}, loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'shop', data: { preload: true}, loadChildren: () => import('./shop/shop.module').then(m => m.ShopModule) },
  { path: 'cart', data: { preload: true}, loadChildren: () => import('./cart/cart.module').then(m => m.CartModule) },
  {path: '**', redirectTo: '/login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
