import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CurrencyService {

  private baseCurrency = 'USD';
  private currencyMap = new Map();

  constructor(
    private http: HttpClient
  ) { }

  getExchangeRate() {
    return this.http.get(`https://api.exchangeratesapi.io/latest?base=${this.baseCurrency}`);
  }

  proceedRates(rates) {

    for (const [key, rate] of Object.entries(rates)) {
      this.currencyMap.set(key, rate);
    }

  }

  convertPrice(totalPrice, currency) {
    const currencyRate = this.currencyMap.get(currency);
    return (currencyRate * totalPrice).toFixed(2);
  }


}
