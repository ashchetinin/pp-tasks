import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Product} from '../interfaces/Product';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  public selectedProductsSource: BehaviorSubject<Array<Product>> = new BehaviorSubject<Array<Product>>([]);
  public selectedProducts$ = this.selectedProductsSource.asObservable();

  constructor() { }

  public get productQuantity() {
    return this.selectedProductsSource.value.length;
  }

  getCartProducts() {

  }
}
