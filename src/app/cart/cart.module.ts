import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import {CartRoutingModule} from './cart-routing.module';
import {CurrencyService} from '../core/services/currency.service';



@NgModule({
  declarations: [CartComponent],
  imports: [
    CommonModule,
    CartRoutingModule
  ],
  providers: [CurrencyService]
})
export class CartModule { }
